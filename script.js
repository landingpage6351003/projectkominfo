document.addEventListener("DOMContentLoaded", function() {
    const announcementPopup = document.getElementById("announcementPopup");
    const openPopupBtn = document.getElementById("openPopupBtn");
    const closePopupBtn = document.getElementById("closePopupBtn");
  
    openPopupBtn.addEventListener("click", function() {
      announcementPopup.classList.add("show");
    });
  
    closePopupBtn.addEventListener("click", function() {
      announcementPopup.classList.remove("show");
    });
  });